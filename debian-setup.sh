#!/bin/bash
# This script sets up the environment for Debian-based systems. Modifications may be necessary
# for other distros/OSs

# Install zsh and other dependencies
sudo apt update && sudo apt upgrade
sudo apt install zsh npm python3-venv tmux

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
if [ -f ~/.zshrc ]; then
  echo "Backing up existing .zshrc file to ~/.zshrc.bak"
  mv ~/.zshrc ~/.zshrc.bak
fi

# Install spaceship ZSH theme
sudo npm install -g spaceship-prompt

# Install autosuggestions plugin
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

# Setup tmux plugin manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Setup tmux
git clone https://github.com/jimeh/tmux-themepack
ln -s $PWD/dotfiles/tmux-shared ~/.tmux.conf

# Setup vim
if [ -f ~/.vimrc ]; then
  echo "Backing up existing .vimrc file to ~/.vimrc.bak"
  mv ~/.vimrc ~/.vimrc.bak
fi
ln -s $PWD/dotfiles/vimrc-shared ~/.vimrc
mkdir -p ~/.vim/colors
cp ./dotfiles/monokai.vim ~/.vim/colors

# Setup nerdtree
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree

cat > ~/.zshrc << EOF
  export ZSH=~/.oh-my-zsh

  if [ -f $PWD/dotfiles/zshrc-active ]; then
    source $PWD/dotfiles/zshrc-active
  fi

  source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
  
  # Place machine-specific ZSH configuration here
EOF

. ~/.zshrc
