# tmux keys
All keys below assume the prefix key, `C-a`, has been pressed first, unless the key is preceeded by an asterisk.

## On the command line
| Command |
| ------ |
| tmux new-session -s session-name |
| tmux ls |
| tmux attach -t session-name |
| tmux kill-session -t target | 

## Sessions
| Key    | Function     |
| ------ |:-------------|
| $      | Rename session |
| L      | Switch to previous session |
| ( )    | Cycle through sessions |
| d      | Detach from session |

## Panes
| Key    | Function     |
| ------ |:-------------|
| o      | Go to next pane |
| &#124;     | Split pane horizontally |
| -      | Split pane vertically |
| * M-Arrows | Move to pane in a direction |
| M-Arrows | Resize pane (repeatable) |
| C-o    | Rotate panes around |
| !      | Create a new window and move current pane to it |
| * C-d  | Kill current pane |
| { }    | Swap current pane with previous/next one |
| z      | Toggle fullscreen zoom of current pane |
| [      | Enter copy/scroll mode (can use vim keys) | 

## Windows
| Key    | Function     |
| ------ |:-------------|
| c      | Create a new window |
| 0...9  | Choose window 0,1,...,9 |
| ,      | Rename current window |
| '      | Select a window (prompt for index) |
| f      | Find window whose title matches a glob |
| &      | Kill the current window |
| n      | Go to next window |
| p      | Go to previous window |
| w      | Choose windows from interactive list |

## Misc
| Key    | Function     |
| ------ |:-------------|
| C-a    | Send prefix to the terminal |
| C-z    | Send tmux to the background |
| :      | Manually enter a command |
| ?      | tmux help |

###### *Based on [tmux cheat sheet v1.0](http://www.clintoncurry.net/tmux-cheatsheet)*
