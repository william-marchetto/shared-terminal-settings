# Shared Terminal Settings Quickstart Guide

Easily share terminal settings across machines by linking to shared config files. By keeping these files outside of your home directory, you can easily make local changes without affecting your shared version. The steps below will walk through how to set this up with my configuration:

![Terminal screenshot](terminal.png?raw=true "Terminal screenshot")

* Download files and place them in ~/dotfiles.

### Setup zsh

* Install [zsh](http://www.zsh.org/) (Homebrew if on Mac).

* Install [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh).

* Install spaceship theme:
``` bash
npm install -g spaceship-prompt
```

* Install autosuggestions plugin:
``` bash
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions 
```

* Import shared ZSH settings from zshrc-shared in ~/.zshrc:
``` bash
export ZSH=~/.oh-my-zsh

if [ -f ~/dotfiles/zshrc-active ]; then
    source ~/dotfiles/zshrc-active
fi

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Place machine-specific ZSH configuration here
```

### Setup tmux

* Setup tmux plugin manager
``` bash 
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

* Symlink tmux.conf
``` bash
ln -s ~/dotfiles/tmux-shared ~/.tmux.conf
```

* Download tmux themepack
``` bash
git clone https://github.com/jimeh/tmux-themepack
```

* [Mac] In Terminal preferences, remove the Alt-(Left Arrow) and Alt-(Right Arrow) keybindings

*See [tmux keys](tmux-keys.md)*

### Setup vim

* Remove existing .vimrc
 
* Symlink vimrc
``` bash
ln -s ~/dotfiles/vimrc-shared ~/.vimrc
```

* Install monokai vim theme
``` bash
mkdir -p ~/.vim/colors
cp ~/dotfiles/monokai.vim ~/.vim/colors
```

* Install Black dependency
``` bash
apt install python3-venv
```

* Install Vundle
``` bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
```

* Install NERDTree
``` bash
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
```

## Other settings

* [Mac] Install iTerm2
* [Mac] Install iTerm2 [monokai theme](https://github.com/stephenway/monokai.terminal)
* Add this to .gitconfig:
```
[alias]
    lg = !"git lg1"
    lg1 = !"git lg1-specific --all"
    lg2 = !"git lg2-specific --all"
    lg3 = !"git lg3-specific --all"

    lg1-specific = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'
    lg2-specific = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)'
    lg3-specific = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(auto)%d%C(reset)%n''          %C(white)%s%C(reset)%n''          %C(dim white)- %an <%ae> %C(reset) %C(dim white)(committer: %cn <%ce>)%C(reset)'
```

## Changing environment setup
My favorite oh-my-zsh theme doesn't work with PuTTY, so I've included scripts that change the zshrc. If using PuTTY, just execute env-putty. To switch back, execute env-unix.
